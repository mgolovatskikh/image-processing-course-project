#include <tinyxml2.h>
#include <string>
#include <iostream>
#include <opencv2/opencv.hpp>
using namespace std;
using namespace tinyxml2;
using namespace cv;
const char * getPoints(XMLNode * root, const char * name, string value){
    XMLNode * cur = root->FirstChildElement("track");
    do {
        if (cur->FirstChildElement("points")->ToElement()->Attribute(name, value.c_str())){
            return cur->FirstChildElement("points")->ToElement()->Attribute("points");
        }
        cur = cur->NextSibling();
    } while (true);
}
Point2f parsePoint(string str){
    int pos = str.find(',');
    string x = str.substr(0, pos);
    string y = str.substr(pos+1);
    return Point2f(atof(x.c_str()), atof(y.c_str()) );
}
vector<Point2f > getPointsDouble(string str){
    vector<Point2f> res;
    size_t pos = 0;
    string point;
    while ((pos = str.find(';')) != string::npos){
        point = str.substr(0, pos);
//        cout << point << endl;
        res.push_back(parsePoint(point));
        str.erase(0, pos + 1);
    };
    res.push_back(parsePoint(str));
    return res;
}
//int main()
//{
//    tinyxml2::XMLDocument xml_doc;
//    xml_doc.LoadFile("../points_1avi.xml");
//    if (xml_doc.ErrorID() == 0){
//        cout<< "ok";
//    }
//    XMLNode * root = xml_doc.FirstChildElement("annotations");
//    if (root == nullptr) return false;
//    XMLElement * element = root->FirstChildElement("track")->FirstChildElement("points")->ToElement();
//    if (element == nullptr) return false;
//    const char * res = getPoints(root,"frame", "6");
//    string r = (string)res;
//    cout << r << endl;
////    int pos = r.find(';');
////    string token = r.substr(0 ,pos);
////    Point2d p = parsePoint(token);
////    cout << p;
//    vector<Point2d > points = getPointsDouble(r);
//    cout << points;
//    return 0;
//}