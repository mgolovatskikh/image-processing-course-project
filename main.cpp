#include <opencv2/opencv.hpp>
#include <iostream>
#include <cassert>
#include <cmath>
#include <fstream>
#include "tinyxml2.h"
#define PI 3.14159265
using namespace std;
using namespace cv;

// This video stablisation smooths the global trajectory using a sliding average window

const int SMOOTHING_RADIUS = 30; // In frames. The larger the more stable the video, but less reactive to sudden panning
const int HORIZONTAL_BORDER_CROP = 40; // In pixels. Crops the border to reduce the black borders from stabilisation being too noticeable.

// 1. Get previous to current frame transformation (dx, dy, da) for all frames
// 2. Accumulate the transformations to get the image trajectory
// 3. Smooth out the trajectory using an averaging window
// 4. Generate new set of previous to current transform, such that the trajectory ends up being the same as the smoothed trajectory
// 5. Apply the new transformation to the video

struct TransformParam
{
    TransformParam() {}
    TransformParam(double _dx, double _dy, double _da) {
        dx = _dx;
        dy = _dy;
        da = _da;
    }

    double dx;
    double dy;
    double da; // angle
};

struct Trajectory
{
    Trajectory() {}
    Trajectory(double _x, double _y, double _a) {
        x = _x;
        y = _y;
        a = _a;
    }

    double x;
    double y;
    double a; // angle
};

int main(int argc, char **argv)
{

    VideoCapture cap("../selhoz/1.avi");
    assert(cap.isOpened());

    Mat cur, cur_grey;
    Mat prev, prev_grey;

    VideoWriter output("1out.avi", cap.get(CAP_PROP_FOURCC),
                       cap.get(CAP_PROP_FPS),
                       cv::Size(cap.get(CAP_PROP_FRAME_WIDTH),
    cap.get(CAP_PROP_FRAME_HEIGHT)));
    while(true) {
        cap >> cur;
        if(cur.data == NULL) {
            break;
        }
        Mat orig = cur;
        cvtColor(cur, cur, COLOR_BGR2GRAY);
        medianBlur(cur, cur, 21);
        Mat edges, adaptiveTresholded;
        //adaptiveThreshold(cur,adaptiveTresholded,255,ADAPTIVE_THRESH_GAUSSIAN_C,THRESH_BINARY,51,15);
        cur = cur > 220;
//       threshold(cur, edges, 208, THRESH_BINARY, THRESH_BINARY);
        Canny(cur, edges, 100, 190);
        vector<vector<Point> > contours;
        vector<Vec4i> hierarchy;
        findContours(edges, contours, hierarchy, RETR_TREE, CHAIN_APPROX_NONE);
        Vec4f Line;
        Mat edges_points = edges == 255;
//        drawContours(orig, contours, -1, Scalar(0, 0, 255), 4);
        fitLine(contours[0], Line, DIST_L2, 0,0.001,0.01);
//        vector<Vec4i> lines;
//        HoughLinesP(edges, lines, 1, 2*CV_PI/180, 100,150, 300 );
//        for( size_t i = 0; i < lines.size(); i++ ) {
            Vec4f l = Line;

            // oran = float(l[1] / col_size );
//            double angle = atan2(l[3] - l[1], l[2] - l[0]) * 180.0 / CV_PI;
//
//            if (angle < 5 && angle >= -5) {
//                //if(1){
                cout << l[0] << "," << l[1] << "," << l[2] << "," << l[3] << endl;
                double angle = atan(l[1] / l[0]) * 180 / PI;
                Point2f pc(orig.cols/2., orig.rows/2.);
                Mat r = getRotationMatrix2D(pc, angle, 1.0);
                Mat rotated;
                warpAffine(orig, rotated, r, orig.size());
                cout << "angle" << angle << endl;
                cv::line(orig, Point(l[2], l[3]), Point(l[2]+ l[0]*300, l[3]+l[1]*300), Scalar(255, 0, 255), 5, LINE_AA);
//            }
//        }
        //imshow("cur", orig);
        imshow("rotated", rotated);
        output.write(rotated);
        waitKey(178);
    }


    return 0;
}